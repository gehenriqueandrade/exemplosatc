﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoSatLinker
{
    class FuncoesDll
    {
        const String cDLL = "dllsat.dll";

        [DllImport(cDLL, EntryPoint = "AtivarSAT", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr AtivarSAT(int numSessao, int subCommand, string codAtivacao, string CNPJCont, int cUF);

        [DllImport(cDLL, EntryPoint = "ConsultarSAT", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ConsultarSAT(int numSessao);

        [DllImport(cDLL, EntryPoint = "ConsultarStatusOperacional", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ConsultarStatusOperacional(int numSessao, string codAtivacao);

        [DllImport(cDLL, EntryPoint = "EnviarDadosVenda", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr EnviarDadosVenda(int numSessao, string codAtivacao, string dadosVenda);

        [DllImport(cDLL, EntryPoint = "CancelarUltimaVenda", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr CancelarUltimaVenda(int numSessao, string codAtivacao, string chave, string dadosCancelamento);

        [DllImport(cDLL, EntryPoint = "GeraNumeroSessao", CallingConvention = CallingConvention.StdCall)]
        public static extern int GeraNumeroSessao();

        [DllImport(cDLL, EntryPoint = "TesteFimAFim", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr TesteFimAFim(int numSessao, string codAtivacao, string dados);

        [DllImport(cDLL, EntryPoint = "TrocarCodigoDeAtivacao", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr TrocarCodigoDeAtivacao(int numSessao, string codAtivacao, int opcao, string cod, string codConf);

        [DllImport(cDLL, EntryPoint = "ConsultarNumeroSessao", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ConsultarNumeroSessao(int numSessao, string codAtivacao, int numero);

        [DllImport(cDLL, EntryPoint = "AssociarAssinatura", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr AssociarAssinatura(int numSessao, string codAtivacao, string CNPJVal, string assCNPJ);

        [DllImport(cDLL, EntryPoint = "AtualizarSoftwareSAT", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr AtualizarSoftwareSAT(int numSessao, string codAtivacao);

        [DllImport(cDLL, EntryPoint = "BloquearSAT", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr BloquearSAT(int numSessao, string codAtivacao);

        [DllImport(cDLL, EntryPoint = "DesbloquearSAT", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr DesbloquearSAT(int numSessao, string codAtivacao);

        [DllImport(cDLL, EntryPoint = "Base64ToAscii", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr Base64ToAscii();

        [DllImport(cDLL, EntryPoint = "ExtrairLogs", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ExtrairLogs(int numSessao, string codAtivacao);

        [DllImport(cDLL, EntryPoint = "ConfigurarInterfaceDeRede", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ConfigurarInterfaceDeRede(int numSessao, string codAtivacao, string configuracao);
    }
}
