﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoSatLinker
{
    public partial class ConfigurarRede : Form
    {
        string xmlrede, retorno;
        int ns;
        public ConfigurarRede()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "DHCP" && comboBox2.Text == "0 - Não uso Proxy")
            {
                xmlrede = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?><config><tipoInter>ETHE</tipoInter><tipoLan>DHCP</tipoLan></config>";
            }
            if (comboBox1.Text == "DHCP" && comboBox2.Text == "1 - Proxy com Configuração")
            {
            xmlrede = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?><config><tipoInter>ETHE</tipoInter><tipoLan>DHCP</tipoLan><proxy>1</proxy><proxy_ip>"+textBox6.Text+"</proxy_ip><proxy_porta>"+textBox8.Text+"</proxy_porta><proxy_user>"+textBox7.Text+"</proxy_user><proxy_senha>"+textBox9.Text+"</proxy_senha></config>";
            }
            if (comboBox1.Text == "DHCP" && comboBox2.Text == "2 - Proxy Transparente")
            {
            xmlrede = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?><config><tipoInter>ETHE</tipoInter><tipoLan>DHCP</tipoLan><proxy>1</proxy><proxy_ip>" + textBox6.Text + "</proxy_ip><proxy_porta>" + textBox8.Text + "</proxy_porta></config>";
            }
            if (comboBox1.Text == "IPFIX" && comboBox2.Text == "0 - Não uso Proxy")
            {
            xmlrede = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?><config><tipoInter>ETHE</tipoInter><SSID>"+textBox10.Text+ "</SSID><seg>"+textBox11.Text+"</seg>"+textBox12.Text+ "<tipoLan>IPFIX</tipoLan><lanIP>"+textBox1.Text+"</lanIP><lanMask>"+textBox2.Text+"</lanMask><lanGW>"+textBox5.Text+"</lanGW><lanDNS1>"+textBox3.Text+"</lanDNS1><lanDNS2>"+textBox4.Text+"</lanDNS2><usuario>"+textBox13.Text+"</usuario><senha>"+textBox14.Text+"</senha></config>";
            }
            if (comboBox1.Text == "IPFIX" && comboBox2.Text == "1 - Proxy com Configuração")
            {
            xmlrede = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?><config><tipoInter>ETHE</tipoInter><SSID>" + textBox10.Text + "</SSID><seg>" + textBox11.Text + "</seg>" + textBox12.Text + "<tipoLan>IPFIX</tipoLan><lanIP>" + textBox1.Text + "</lanIP><lanMask>" + textBox2.Text + "</lanMask><lanGW>" + textBox5.Text + "</lanGW><lanDNS1>" + textBox3.Text + "</lanDNS1><lanDNS2>" + textBox4.Text + "</lanDNS2><usuario>" + textBox13.Text + "</usuario><senha>" + textBox14.Text + "</senha><proxy>1</proxy><proxy_ip>" + textBox6.Text + "</proxy_ip><proxy_porta>" + textBox8.Text + "</proxy_porta><proxy_user>" + textBox7.Text + "</proxy_user><proxy_senha>" + textBox9.Text + "</proxy_senha></config>";
            }
            if (comboBox1.Text == "IPFIX" && comboBox2.Text == "2 - Proxy Transparente")
            {
            xmlrede = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?><config><tipoInter>ETHE</tipoInter><SSID>" + textBox10.Text + "</SSID><seg>" + textBox11.Text + "</seg>" + textBox12.Text + "<tipoLan>IPFIX</tipoLan><lanIP>" + textBox1.Text + "</lanIP><lanMask>" + textBox2.Text + "</lanMask><lanGW>" + textBox5.Text + "</lanGW><lanDNS1>" + textBox3.Text + "</lanDNS1><lanDNS2>" + textBox4.Text + "</lanDNS2><usuario>" + textBox13.Text + "</usuario><senha>" + textBox14.Text + "</senha><proxy>1</proxy><proxy_ip>" + textBox6.Text + "</proxy_ip><proxy_porta>" + textBox8.Text + "</proxy_porta></config>";
            }

            ns = FuncoesDll.GeraNumeroSessao();
            retorno = Marshal.PtrToStringAnsi(FuncoesDll.ConfigurarInterfaceDeRede(ns, textBox15.Text, xmlrede));
            textBox16.Text = retorno;
        }
    }
}
