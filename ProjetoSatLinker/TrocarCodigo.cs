﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoSatLinker
{
    public partial class TrocarCodigo : Form
    {
        int cod;
        public TrocarCodigo()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox1.Items.Add("1 - Código de Ativação.");
            comboBox1.Items.Add("2 - Código de Ativação de Emergência.");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "1 - Código de Ativação")
            {
                cod = 1;
            }
            if (comboBox1.Text == "2 - Código de Ativação de Emergência")
            {
                cod = 2;
            }

            if (comboBox1.Text == "" || textBox1.Text == "" || textBox2.Text == "")
            {
                textBox3.Text = "Há informações faltando. Favor verificar campos assina.";
            }
            else
            {
                int ns = FuncoesDll.GeraNumeroSessao();
                String retorno = Marshal.PtrToStringAnsi(FuncoesDll.TrocarCodigoDeAtivacao(ns, textBox4.Text, cod, textBox1.Text, textBox2.Text));
                textBox3.Text = retorno;
            }
        }
    }
}
