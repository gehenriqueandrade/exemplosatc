﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace ProjetoSatLinker
{
    public partial class Form1 : Form
    {
        int ns;
        String retorno;
        String[] ret,ret2,logg;

        // FORM PRINCIPAL
        public Form1()
        {
            InitializeComponent();
        }
        // CONSULTAR SAT
        private void button1_Click(object sender, EventArgs e)
        {
            ns = FuncoesDll.GeraNumeroSessao();
            retorno = Marshal.PtrToStringAnsi(FuncoesDll.ConsultarSAT(ns));
            textBox7.Text = retorno;
        }
        // CONSULTAR ESTADO OPERACIONAL
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox7.Text = "Por favor, informe o código de ativação.";
            }
            else
            {
            ns = FuncoesDll.GeraNumeroSessao();
            retorno = Marshal.PtrToStringAnsi(FuncoesDll.ConsultarStatusOperacional(ns,textBox1.Text));
            ret = retorno.Split('|');
            textBox7.Text = "E   S   T   A   D   O       O   P   E   R   A   C   I   O   N   A   L" + System.Environment.NewLine + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Número de Sessão: " + ret[0] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Código EEEEE: " + ret[1] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Mensagem: " + ret[2] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Código de Referência: " + ret[3] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Mensagem Sefaz: " + ret[4] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Número de Série: " + ret[5] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Tipo de LAN: " + ret[6] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "LAN IP: " + ret[7] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "LAN MAC: " + ret[8] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "LAN MASK: " + ret[9] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "LAN GW: " + ret[10] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "LAN DNS 1: " + ret[11] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "LAN DNS 2: " + ret[12] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "STATUS LAN: " + ret[13] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Nível De Bateria: " + ret[14] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Memória Total: " + ret[15] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Memória em Uso: " + ret[16] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Data & Hora: " + ret[17] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Versão do Software: " + ret[18] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Versão do Layout: " + ret[19] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Ultimo CFe emitido: " + ret[20] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Primeiro CFe emitido: " + ret[21] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Data e Hora da ultima transmissão: " + ret[22] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Ultima comunicação com a SEFAZ: " + ret[23] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Certificado: " + ret[24] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Vencimento do Certificado: " + ret[25] + System.Environment.NewLine + System.Environment.NewLine;
            textBox7.Text = textBox7.Text + "Status de Operação: " + ret[26] + System.Environment.NewLine + System.Environment.NewLine;
            }
        }
        // ATIVAR EQUIPAMENTO
        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                textBox7.Text = "Há informações pendentes, favor verificar.";
            }
            else
            {
                ns = FuncoesDll.GeraNumeroSessao();
                retorno = Marshal.PtrToStringAnsi(FuncoesDll.AtivarSAT(ns, 1, textBox1.Text, textBox2.Text, 35));
                textBox7.Text = retorno;
            }
        }
        // VINCULAR AC
        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox3.Text == "" || textBox4.Text == "")
            {
                textBox7.Text = "Há informações pendentes, favor verificar.";
            }
            else
            {
                ns = FuncoesDll.GeraNumeroSessao();
                retorno = Marshal.PtrToStringAnsi(FuncoesDll.AssociarAssinatura(ns, textBox1.Text, textBox3.Text, textBox4.Text));
                textBox7.Text = retorno;
            }
        }
        // TESTE FIM-A-FIM
        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox10.Text == "")
            {
                textBox7.Text = "Há informações pendentes, favor verificar.";
            }
            else
            {
                XDocument xmlfimafim;
                xmlfimafim = XDocument.Load(textBox10.Text);
                retorno = Marshal.PtrToStringAnsi(FuncoesDll.TesteFimAFim(ns, textBox1.Text, xmlfimafim.ToString()));
                textBox7.Text = retorno;
            }
        }
        // BOTÃO SELEÇÃO XML DE VENDA
        private void button15_Click(object sender, EventArgs e)
        {
            OpenFileDialog xmlvenda = new OpenFileDialog();
            xmlvenda.Title = "Select XML";
            xmlvenda.DefaultExt = "xml";
            xmlvenda.Filter = "xml Files (*.xml)|*.xml| All Files (*.*)|*.*";
            xmlvenda.ShowDialog();
            textBox8.Text = xmlvenda.FileName;
        }
        // EMISSÃO DE VENDA
        private void button12_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox8.Text == "")
            {
                textBox7.Text = "Há informações pendentes, favor verificar.";
            }
            else
            {
                XDocument xmlvenda;
                xmlvenda = XDocument.Load(textBox8.Text);
                retorno = Marshal.PtrToStringAnsi(FuncoesDll.EnviarDadosVenda(ns, textBox1.Text, xmlvenda.ToString()));
                textBox7.Text = retorno;
            }
        }
        // BOTÃO SELEÇÃO FIM-A-FIM
        private void button17_Click(object sender, EventArgs e)
        {
            OpenFileDialog xmlfimafim = new OpenFileDialog();
            xmlfimafim.Title = "Select XML";
            xmlfimafim.DefaultExt = "xml";
            xmlfimafim.Filter = "xml Files (*.xml)|*.xml| All Files (*.*)|*.*";
            xmlfimafim.ShowDialog();
            textBox8.Text = xmlfimafim.FileName;
        }
        // BOTÃO SELEÇÃO XML CANCELAMENTO
        private void button16_Click(object sender, EventArgs e)
        {
            OpenFileDialog xmlcancelamento = new OpenFileDialog();
            xmlcancelamento.Title = "Select XML";
            xmlcancelamento.DefaultExt = "xml";
            xmlcancelamento.Filter = "xml Files (*.xml)|*.xml| All Files (*.*)|*.*";
            xmlcancelamento.ShowDialog();
            textBox9.Text = xmlcancelamento.FileName;
        }
        // CANCELAR VENDA
        private void button13_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox9.Text == "")
            {
                textBox7.Text = "Há informações pendentes, favor verificar.";
            }
            else
            {
                XDocument xmlcancelar;
                xmlcancelar = XDocument.Load(textBox9.Text);
                retorno = Marshal.PtrToStringAnsi(FuncoesDll.EnviarDadosVenda(ns, textBox1.Text, xmlcancelar.ToString()));
                textBox7.Text = retorno;
            }
        }
        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }
        // BLOQUEAR EQUIPAMENTO
        private void button8_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox7.Text = "Por favor, informe o código de ativação.";
            }
            else
            {
                ns = FuncoesDll.GeraNumeroSessao();
                retorno = Marshal.PtrToStringAnsi(FuncoesDll.BloquearSAT(ns, textBox1.Text));
                textBox7.Text = retorno;
            }
        }
        // DESBLOQUEAR EQUIPAMENTO
        private void button9_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox7.Text = "Por favor, informe o código de ativação.";
            }
            else
            {
                ns = FuncoesDll.GeraNumeroSessao();
                retorno = Marshal.PtrToStringAnsi(FuncoesDll.DesbloquearSAT(ns, textBox1.Text));
                textBox7.Text = retorno;
            }
        }
        // ATUALIZAR EQUIPAMENTO
        private void button11_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox7.Text = "`Por favor, informe o código de ativação.";
            }
            else
            {
                ns = FuncoesDll.GeraNumeroSessao();
                retorno = Marshal.PtrToStringAnsi(FuncoesDll.AtualizarSoftwareSAT(ns, textBox1.Text));
                textBox7.Text = retorno;
            }
        }
        // TROCAR CODIGO DE ATIVAÇÃO
        private void button10_Click(object sender, EventArgs e)
        {
            TrocarCodigo formtrocacodigo = new TrocarCodigo();
            formtrocacodigo.Show();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        // CONSULTAR NUMERO SESSÃO
        private void button14_Click(object sender, EventArgs e)
        {
            ns = FuncoesDll.GeraNumeroSessao();
            int nsconsul = ns - 1;
            retorno = Marshal.PtrToStringAnsi(FuncoesDll.ConsultarNumeroSessao(ns, textBox1.Text, nsconsul));
            textBox7.Text = retorno;
        }
        // CONFIGURAR REDE
        private void button7_Click(object sender, EventArgs e)
        {
            ConfigurarRede formrede = new ConfigurarRede();
            formrede.Show();
        }
        // EXTRAIR LOG
        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox7.Text = "Há informações pendentes, favor verificar.";
            }
            else
            {
                ns = FuncoesDll.GeraNumeroSessao();
                retorno = Marshal.PtrToStringAnsi(FuncoesDll.ExtrairLogs(ns, textBox1.Text));
                logg = retorno.Split('|');
                String meuovo = logg[5];
                meuovo.ToString();
                String meuovodois = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(meuovo));
                StreamWriter log = new StreamWriter("C:\\git\\log.xml", true, Encoding.ASCII);
                log.Write(meuovodois);
                textBox7.Text = "•     L     O     G     •" + System.Environment.NewLine + System.Environment.NewLine + System.Environment.NewLine;
                textBox7.Text = textBox7.Text + "Status do Retorno: " + logg[0] + "|" + logg[1] + "|" + logg[2] + "|" + logg[3] + "|" + logg[4] + System.Environment.NewLine + System.Environment.NewLine;
                textBox7.Text = textBox7.Text + "O LOG do equipamento foi criado na sua pasta do projeto! O nome do arquivo é: log.txt";
            }
        }
    }
}